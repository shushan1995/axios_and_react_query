import axios from "axios";
import Tutorial from "../types/Tutorial";

const apiClient = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/posts",
  headers: {
    "Content-type": "application/json",
  },
});

const findAll = async () => {
  const response = await apiClient.get<Tutorial[]>("/");
  return response.data;
}

const findById = async (id: any) => {
  const response = await apiClient.get<Tutorial>(`/${id}`);
  return response.data;
}

const findByTitle = async (title: string) => {
  const response = await apiClient.get<Tutorial[]>(`/title=${title}`);
  return response.data;
}

const create = async ({ title, body }: Tutorial) => {
  const response = await apiClient.post<any>("/tutorials", { title, body });
  return response.data;
}

const update = async (id: any, { title, body, published }: Tutorial) => {
  const response = await apiClient.put<any>(`/tutorials/${id}`, { title, body, published });
  return response.data;
}

const deleteById = async (id: any) => {
  const response = await apiClient.delete<any>(`/tutorials/${id}`);
  return response.data;
}

const deleteAll = async () => {
  const response = await apiClient.delete<any>("/tutorials");
  return response.data;
}

const TutorialService = {
  findAll,
  findById,
  findByTitle,
  create,
  update,
  deleteById,
  deleteAll
}

export default TutorialService;