export default interface Tutorial {
  id?: any | null,
  title: string,
  body: string,
  published?: boolean,
}